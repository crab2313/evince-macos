all: evince.dmg


evince.dmg: evince.app
	sh dmg.sh

evince.app: evince.bundle Info.plist Evince.icns launcher.sh
	rm -rf evince.app
	gtk-mac-bundler evince.bundle

clean:
	rm -rf evince.app evince.dmg


.PHONY: all clean
