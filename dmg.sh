#!/bin/sh

test -f evince.dmg && rm evince.dmg
create-dmg \
  --volname "Evince Installer" \
  --volicon "Evince.icns" \
  --window-pos 200 120 \
  --window-size 800 400 \
  --icon-size 100 \
  --icon "evince.app" 200 190 \
  --hide-extension "evince.app" \
  --app-drop-link 600 185 \
  "evince.dmg" \
  "evince.app/"
